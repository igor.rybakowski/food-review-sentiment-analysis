## Food Review Sentiment Analysis

# Wstęp

Założeniem projektu jest stworzenie klasyfikatora recenzji jedzenia, badającego czy 
dana recenzja jest pozytywna czy negatywna.


# Zbiór danych

Jako zbiór danych wykorzystaliśmy baze recenzji 'Amazon Fine Food Reviews' z portalu kaggle.com

https://www.kaggle.com/snap/amazon-fine-food-reviews


Ze względów wydajnościowych zmniejszyliśmy ilość danych treningowych poprzez komendę .drop() 

```python
df.drop(df.tail(500000).index,inplace=True)
```

# Modyfikacja ramki danych

Po załadowaniu pliku .csv, dodaliśmy kolumnę 'sentiment' i w pętli do każdej recenzji została dodana
wartość 0 lub 1 na podstawie obecnej już tam punktacji 'score'. Od 0 do 3 - negatywna recenzja,
4 i 5 to recenzje pozytywne. Po tym kroku pozbyliśmy się zbędnych kolumn, zostawiając tylko 'Sentiment'
oraz samą recenzję w kolumnie 'Text'

 
```python
df['Sentiment'] = -1
for i,row in df.iterrows():
    val = 0
    if df.at[i,'Score'] > 3:
        val = 1
    df.at[i, 'Sentiment'] = val
```
# Kalibracja danych

Po przypisaniu każdej recenzji wartości 0 lub 1 posortowaliśmy ten zbiór i usunęliśmy znaczną ilość pozytywnych recenzji
ze względu na ich zbyt dużą ilość w porównaniu do negatywnych recenzji. 

```python
df = df.sort_values(by=['Sentiment'])
df.drop(df.tail(30000).index,inplace=True)
```

# Przygotowanie danych (preprocessing)

W kolejnym kroku podjęliśmy się obróbki recenzji z naszej ramki danych. Każda recenzja została pozbawiona
dużych liter, specjalnych znaków, słów ze stop listy, często występujących słów oraz tych rzadkich.

# Ekstrakcja danych

Poprzez funkcje zawarte w tej sekcji stworzyliśmy osobne kolumny, w których przechowywane są informacje
o sentencjach. Ilość słów, znaków i średnia długość słowa. 

# Wektoryzacja danych 

W tym kroku następuje transformacja tekstu do postaci wektora. 

# Trenowanie modelu

Następuje tutaj przydział danych do kolumn X(20% na testy) i y(80% na trening)


```python
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
```

W tej sekcji zaimportowaliśmy SVM (support vector machine), który jest algorytmem używanym w 
machine learning do klasyfikacji.


# Classification report 

Jak widać nasza maszyna posiada całkiem wysoką trafność w odgadywaniu pozytywności recenzji

```python
              precision    recall  f1-score   support

           0       0.79      0.74      0.76      3094
           1       0.83      0.87      0.85      4597

    accuracy                           0.82      7691
   macro avg       0.81      0.80      0.81      7691
weighted avg       0.81      0.82      0.81      7691
```


# Unit testing

Za pomocą biblioteki 'unit', dodaliśmy testy jednostkowe. Poprzez stworzenie klasy 'Review' i 'TestReview', mamy możliwość
sprawdzenia czy nasz model zgadnie czy recenzja wprowadzona przez nas poprzez input() jest pozytywna lub negatywna na podstawie wcześniejszego trenowania. 
